const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('services');
});

router.get('/cloud-services', function(req, res, next) {
    res.render('service-cloud-services');
});

router.get('/network-security', function(req, res, next) {
    res.render('service-network-security');
});

router.get('/software-development', function(req, res, next) {
    res.render('service-software-development');
});

router.get('/data-management', function(req, res, next) {
    res.render('service-data');
});

router.get('/devops', function(req, res, next) {
    res.render('service-devops');
});

module.exports = router;
