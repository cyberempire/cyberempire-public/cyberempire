const express = require("express")
const path = require("path")
const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public', 'css')));


// routers
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users'); //not used
const aboutRouter = require('./routes/about');
const servicesRouter = require('./routes/services');

// urls
app.use('/', indexRouter);
app.use('/users', usersRouter); //not used
app.use('/about', aboutRouter);
app.use('/services', servicesRouter);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     next(createError(404));
// });

const PORT = 3000
app.listen(PORT, () => {
    console.log("Listening on port http://localhost:" + PORT)
})

module.exports = app;
